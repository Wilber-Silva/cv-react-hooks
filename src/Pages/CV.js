import React, { Component } from 'react'
import { connect } from 'react-redux'

import { stopLoaging } from './../Actions/ApplicationLoading'

import CvContainer from './../Components/Containers/Cv'

class CV extends Component {
    componentDidMount () {
        setTimeout(() => this.props.stopLoaging(), 5000)
    }
    render(){
        return (
            <CvContainer>
            
            </CvContainer>
        )
    }
}

const mapStateToProps = ({applicationLoading}) => ({
    applicationLoading
})

const mapDispathToProps = (dispatch) => ({
    stopLoaging: applicationLoading => dispatch(stopLoaging())
})

export default connect(
    mapStateToProps,
    mapDispathToProps
)(CV)