import { combineReducers } from 'redux'

import ApplicationLoadingReducer from './ApplicationLoading'

export default combineReducers({
    applicationLoading: ApplicationLoadingReducer
})