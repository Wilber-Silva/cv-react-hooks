import { TYPES } from './../Actions/ApplicationLoading'

const initialState = {
    loading: true
}
  
export default (state = initialState, action) => {
    switch (action.type) {
        case TYPES.START_LOADING:
            return {
            ...state,
            loading: true
            }
        case TYPES.STOP_LOADING:
            return {
                ...state,
                loading: false
            }
        default:
            return state
    }
}