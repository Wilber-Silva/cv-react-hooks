import React from 'react'
import { BrowserRouter, Switch, Route } from "react-router-dom"

import CVPage from "./Pages/CV"

export default () => (
    <BrowserRouter>
        <Switch>
          <Route path="/" component={CVPage}/>
        </Switch>
    </BrowserRouter>
)