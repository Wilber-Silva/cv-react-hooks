// Moked skills

const nodejsDuration = '4 anos'
const fullTime = '6 anos'
const reactjsDuration = '2 anos'
const agileDuration = '3 anos'
const awsDration = '4 anos'

export default [
    // Develop
    { iconSrc: '/image/icons/js.png', name: 'JavaScript', points: 10, duration: fullTime, group: 'language' },
    { iconSrc: '/image/icons/php.png', name: 'PHP', points: 10, duration: fullTime, group: 'language' },
    { iconSrc: '/image/icons/html.png', name: 'HTML', points: 10, duration: fullTime, group: 'language' },
    { iconSrc: '/image/icons/css.png', name: 'CSS', points: 8, duration: fullTime, group: 'language' },

    // ---
    { iconSrc: '/image/icons/nodejs.png', name: 'NodeJs', points: 10, duration: nodejsDuration, group: 'develop' },
    { iconSrc: '/image/icons/react.png', name: 'React', points: 8, duration: '2 anos e 5 meses', group: 'develop' },
    { iconSrc: '/image/icons/react-native.jpg', name: 'React Native', points: 5, duration: reactjsDuration, group: 'develop' },
    { iconSrc: '/image/icons/sass.png', name: 'SASS', points: 8, duration: reactjsDuration, group: 'develop' },
    // ---
    { iconSrc: '/image/icons/expressjs.png', name: 'ExpressJs', points: 10, duration: nodejsDuration, group: 'framework' },
    { iconSrc: '/image/icons/laravel.png', name: 'Laravel PHP', points: 10, duration: '4 anos e 8 meses', group: 'framework' },
    { iconSrc: '/image/icons/momentjs.png', name: 'Moment Js', points: 10, duration: nodejsDuration, group: 'framework' },
    { iconSrc: '/image/icons/lodashjs.png', name: 'Lodash Js', points: 10, duration: nodejsDuration, group: 'framework' },

    // ORM

    { iconSrc: '/image/icons/mongoosejs.jpg', name: 'Mongoose Js', points: 10, duration: nodejsDuration, group: 'orm' },
    { iconSrc: '/image/icons/sequelizejs.png', name: 'Sequelize Js', points: 10, duration: nodejsDuration, group: 'orm' },
    { iconSrc: '/image/icons/eloquentphp.png', name: 'Eloquent PHP', points: 10, duration: nodejsDuration, group: 'orm' },

    
    // Database

    { iconSrc: '/image/icons/mysql.png', name: 'MySql', points: 10, duration: fullTime, group: 'db' },
    { iconSrc: '/image/icons/mongodb.png', name: 'MongoDB', points: 9, duration: nodejsDuration, group: 'db' },
    { iconSrc: '/image/icons/elasticsearch.png', name: 'Elasticsearch', points: 5, duration: '2 anos e 5 meses', group: 'db' },

    { iconSrc: '/image/icons/redis.png', name: 'Redis', points: 8, duration: nodejsDuration, group: 'db' },

    // Tests

    { iconSrc: '/image/icons/nightwatchjs.png', name: 'Nightwatch Js', points: 7, duration: reactjsDuration, group: 'test' },
    { iconSrc: '/image/icons/jest-js.png', name: 'Jest Js', points: 7, duration: reactjsDuration, group: 'test' },

    // Version Control

    { iconSrc: '/image/icons/git.png', name: 'Git', points: 10, duration: fullTime, group: 'versionControl' },
    { iconSrc: '/image/icons/github.png', name: 'Github', points: 6, duration: fullTime, group: 'versionControl' },
    { iconSrc: '/image/icons/bitbucket.jpg', name: 'Bitbucket', points: 8, duration: fullTime, group: 'versionControl' },

    // Concept

    { iconSrc: '/image/icons/microservices.jpeg', name: 'Micro-Serviços', points: 10, duration: nodejsDuration, group: 'concepts' },
    { iconSrc: '/image/icons/devops.png', name: 'DevOPS', points: 7, duration: nodejsDuration, group: 'concepts' },
    { iconSrc: '/image/icons/gitflow.png', name: 'GitFlow', points: 7, duration: nodejsDuration, group: 'concepts' },
    { iconSrc: '/image/icons/apirest.png', name: 'Api Rest', points: 7, duration: fullTime, group: 'concepts' },

    // Infrastructure

    { iconSrc: '/image/icons/nginx.png', name: 'Nginx', points: 7, duration: awsDration, group: 'infrastructure' },
    { iconSrc: '/image/icons/aws-s3.jpg', name: 'AWS S3', points: 10, duration: awsDration, group: 'infrastructure' },
    { iconSrc: '/image/icons/aws-apigateway.png', name: 'AWS Api Gateway', points: 8, duration: awsDration, group: 'infrastructure' },
    { iconSrc: '/image/icons/aws-ses.png', name: 'AWS SES', points: 7, duration: awsDration, group: 'infrastructure' },
    { iconSrc: '/image/icons/aws-sqs.png', name: 'AWS SQS', points: 7, duration: awsDration, group: 'infrastructure' },
    { iconSrc: '/image/icons/aws-lambda.png', name: 'AWS Lambda', points: 6, duration: awsDration, group: 'infrastructure' },
    { iconSrc: '/image/icons/aws-amplify.png', name: 'AWS Amplify', points: 6, duration: awsDration, group: 'infrastructure' },
    { iconSrc: '/image/icons/aws-sns.png', name: 'AWS SNS', points: 6, duration: awsDration, group: 'infrastructure' },

    // Agile 

    { iconSrc: '/image/icons/jira.jpg', name: 'Jira', points: 8, duration: agileDuration, group: 'agile' },
    { iconSrc: '/image/icons/trello.png', name: 'Trello', points: 8, duration: agileDuration, group: 'agile' },
    { iconSrc: '/image/icons/scrum.png', name: 'Scrum', points: 8, duration: agileDuration, group: 'agile' },

    // Others

    { iconSrc: '/image/icons/postman.png', name: 'Postman', points: 10, duration: fullTime, group: 'others' },
    { iconSrc: '/image/icons/mvc.png', name: 'MCV', points: 10, duration: '4 anos', group: 'others' },
    { iconSrc: '/image/icons/mvvm.png', name: 'MVVM', points: 10, duration: '4 anos', group: 'others' },
    { iconSrc: '/image/icons/poo.png', name: 'POO', points: 10, duration: '4 anos', group: 'others' },
    { iconSrc: '/image/icons/tdd.png', name: 'TDD', points: 8, duration: fullTime, group: 'others' },
    { iconSrc: '/image/icons/bdd.png', name: 'BDD', points: 10, duration: fullTime, group: 'others' },
]