import React from 'react';
import { Card, CardHeader, CardContent } from '@material-ui/core'

export default ({ width, avatar, title, content, subtitle, className }) => 
(
    <Card style={{ width, padding: 10, margin: 10 }} className={className}>
        <CardHeader avatar={avatar} 
                    title={title}
                    subheader={subtitle}
        />
        
        <CardContent>
            {content}
        </CardContent>
    </Card>
)
