import React from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { Paper, Grid, Typography, Avatar } from '@material-ui/core'

import CardSkill from './Cards/CardSkill'

import Progress from './Progress/ProgressLevel'
import Load from './Load/Skills'
import skills from './../Mock/skills'

const skillWidth = 235

const useStyles = makeStyles((theme) => (
    {
        paper: {
            width: '100%',
            minHeight: skillWidth,
            borderRadius: 0,
            padding: 50,
            overflow: 'hidden',
            [theme.breakpoints.down('md')]: {
                padding:25
            }
        },
        title: { 
            borderBottom: '3px solid #87CEEB'
        },
        subTitle: {
            fontSize: 17
        },
        skillsContainer: {
            width: '100%',
            margin: 10
        },
        skills: {
            float:'left'
            // position: 'relative',
            // top: 0  
        }
    }
))

const groups = [
    { title: 'Linguagem', name: 'language' },
    { title: 'Desenvolvimento', name: 'develop' },
    { title: 'Framework', name: 'framework' },
    { title: 'ORM', name: 'orm' },
    { title: 'Data Base e Cache', name: 'db' },
    { title: 'Controle de Versão', name: 'versionControl' },
    { title: 'Infraestrutura', name: 'infrastructure' },
    { title: 'Conceitos e praticas', name: 'concepts' },
    { title: 'Metodologia Ágio', name: 'agile' },
    { title: 'Outras', name: 'others' }    
]

const Loading = () => skills.map((skill, key) => <Load key={key} width={skillWidth} />)

const Skills = ({className:classes}) => {
    return groups.map((group, key) => (
        <div key={key} className={classes.skillsContainer}>
            <Typography variant='h5'> {group.title} </Typography>
            <br></br>
            {
                skills
                    .filter(skill => skill.group === group.name)
                    .map((skill, index) => (
                        <CardSkill
                            key={index}
                            className={classes.skills}
                            width={skillWidth}
                            avatar={<Avatar alt={skill.name} src={skill.iconSrc}/>}
                            title={skill.name}
                            subtitle={skill.duration}
                            content={<Progress point={skill.points}/>}
                        />
                    ))
            }
        </div>
    ))
}


const ComponentSkills = ({ loading }) => {
    const classes = useStyles()

    return (
        <Paper className={classes.paper}>
            <Grid container direction='column' alignItems='center' spacing={5}> 
                <Grid item xs sm>
                    <Typography variant='h4' style={{textAlign: 'center'}}>
                        <span className={classes.title}> Habilidades </span>
                        <br></br>
                        <span className={classes.subTitle}>A pontuação varia de 1 até 10</span>
                    </Typography>
                </Grid>
                <Grid item xs sm container direction={'row'} alignItems='center' spacing={2}>
                    {
                        loading ? 
                            <Loading />
                            :
                            <Skills className={classes}/>

                    }
                </Grid>
            </Grid>
        </Paper>
    )
}

const mapStateToProps = ({applicationLoading}) => ({
    loading: applicationLoading.loading
})

export default connect(mapStateToProps)(ComponentSkills)