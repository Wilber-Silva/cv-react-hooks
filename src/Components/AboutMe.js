import React from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { Paper, Grid, Typography } from '@material-ui/core'
import { WhatsApp, Mail } from '@material-ui/icons'

import LoadArea from './Load/Area'
import LoadText from './Load/LineText'

const useStyles = makeStyles((theme) => (
    {
        paper: {
            width: '100%',
            minHeight: 100,
            //marginTop: 10,
            borderRadius: 0,
            padding: 50,
            [theme.breakpoints.up('md')]: {
                paddingLeft: 100
            },
            backgroundColor: 'rgb(43, 43, 43)'
        },
        row: {
            marginBottom: 20
        },
        titleContainer: {
            color: '#FFF'
        },
        contentContainer: {
            color: 'rgb(122, 122, 122)'
        },
        loading: {
            width: '100%'
        }
    }
))

const Loading = ({ className: classes }) => {
    const lineHeight = 25
    return (
        <Grid container direction='column' alignItems='center' spacing={2}>
            <Grid item xs={12} className={classes.loading}>
                <LoadArea height={40}/>
            </Grid>
            <Grid item xs={12} className={classes.loading}>
                <LoadText height={lineHeight}/>
            </Grid>
            <Grid item xs={12} className={classes.loading}>
                <LoadArea height={60}/>
            </Grid>
            <Grid item xs={12} className={classes.loading}>
                <LoadArea height={80}/>
            </Grid>
            <Grid item xs={12} className={classes.loading}>
                <LoadText height={lineHeight}/>
            </Grid>
            <Grid item xs={12} className={classes.loading}>
                <LoadArea height={40}/>
            </Grid>
            <Grid item xs={12} className={classes.loading}>
                <LoadText height={lineHeight}/>
            </Grid>
            <Grid item xs={12} className={classes.loading}>
                <LoadArea height={60}/>
            </Grid>
            <Grid item xs={12} className={classes.loading}>
                <LoadArea height={80}/>
            </Grid>
            <Grid item xs={12} className={classes.loading}>
                <LoadText height={lineHeight}/>
            </Grid>
        </Grid>
    )
}

const AboutMe = ({ className: classes }) => (
    <Grid container direction="column" alignItems='center'>
        <Grid item container className={classes.row} direction='row' alignItems='center' spacing={7}>
            <Grid item xs={12} sm={2} className={classes.titleContainer}>
                <Typography variant='h4'>
                    Sobre min
                </Typography>
            </Grid>
            <Grid item xs={12} sm={10} className={classes.contentContainer}>
                <Typography variant='subtitle1'>
                    Sou desenvolvedor full-stack possuo conhecimentos em web, android e mobile-hybrid, amante de automobilismo e tecnologia com institos de lider 
                    ( costumo contestar as coisas até que chegue em um ponto melhor ou que o atual faça sentido pra min) sempre que possível me intrometo como se eu fosse o dono do produto.
                    <br></br> <br></br>
                    Me didico sempre ao maxímo estou sempre estudando e treinando minhas habilidades em diverssos pontos da vida. ( atualmente minha volta mais rápida em interlagos é de 1.36 minha meta é 1.29)
                    <br></br> <br></br>
                    Sou extremanmente versátil não estudo apenas TI, mas também maketing digital, finanças, mecanica de carros, o mercado pois creio que
                    assim consigo alcançar um maior intendimento do produto e entregar melhor as features.
                    <br></br> <br></br>
                    Acredito que nada é escrito em pedras ou que exista bala de prata, nem sempre uma mesma solução se encaixa em determinado problema,
                    nem sempre uma estrutura bonita na tecnologia é a melhor solução para determinado momento.
                    <br></br> <br></br>
                    Apesar de ser Full-Stack tenho mais conforto no back-end, por conta de ter um leve daltonismo e não ter senso estético preciso estar 
                    sempre mostrando a alguem meus front-ends pra ter certeza que não estou fazendo nada errado (a não ser que tenha um XD pra seguir)
                    <br></br><br></br>
                    Geralmente em meus projetos uso NodeJs ou PHP, serverless, React, React-Native Redux, Git, AWS, MongoDB ou MySql, mas como disse não existe
                    bala de prata não me prendo a ferramenta ou tecnologia eu sou exatamente o cara que se um dia me falarem:
                    <br></br> - "Vamos desenvolver algo com essa tecnologia que você nunca viu?"
                    <br></br> Com tanto que me apresentar bons argumentos eu diria:
                    <br></br> - "Sim claro, vamos lá" 
                </Typography>
            </Grid>
        </Grid>
        <Grid item container className={classes.row} direction='row' alignItems='center' spacing={7}>
            <Grid item xs={12} sm={2} className={classes.titleContainer}>
                <Typography variant='h4'>
                    Contato
                </Typography>
            </Grid>
            <Grid item xs={12} sm={10} className={classes.contentContainer}>
                <Typography variant='subtitle1'>
                    <WhatsApp fontSize='small' /> +55 (11) 9 8789-5615
                    <br></br>
                    <Mail fontSize='small' /> <b>wilberchaves@gmail.com</b>
                </Typography>
            </Grid>
        </Grid>
    </Grid>
)

const ComponentAboutMe = ({ loading }) => {
    const classes = useStyles()

    return (
        <Paper className={classes.paper}>
            {
                loading?
                    <Loading className={classes}/>
                    : 
                    <AboutMe className={classes}/>
            }
        </Paper>
    )
}

const mapStateToProps = ({applicationLoading}) => ({
    loading: applicationLoading.loading
})

export default connect(mapStateToProps)(ComponentAboutMe)