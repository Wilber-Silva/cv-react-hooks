import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid } from '@material-ui/core'

import Title from './../Title'
import AboutMe from './../AboutMe'
import Skills from './../Skills'


const useStyles = makeStyles(() => ({
            root: {
                width: '100%',
                padding: 0,
                left: 0,
                top: 0,
                right: 0
            }
        }
    )
)

export default  ({ children }) => {
    const classes = useStyles()
    return (
        <Grid container className={classes.root}>
            <Title />
            <AboutMe />
            <Skills/>

            { children }
        </Grid>
    )
}

