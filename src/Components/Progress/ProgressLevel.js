import React from 'react'
import { Typography } from '@material-ui/core'
import { ArrowDropDownCircleTwoTone as Circle } from '@material-ui/icons'

const levels = [1,2,3,4,5,6,7,8,9,10]

export default ({ point = 0 }) => {
    let painted = 0
    return (
        <Typography variant="subtitle1">
            {
                levels.map(level => {
                        if (painted < point) {
                            painted++
                            return <Circle key={level} fontSize="small" style={{color:'#00FF7F'}}/>
                        }
                        return <Circle key={level} fontSize='small' style={{color:'#C0C0C0'}} />
                    }
                )
            }
        </Typography>
    )
}