import React from 'react'
import Skeleton from '@material-ui/lab/Skeleton'

export default ({ width, height }) => <Skeleton variant="text" height={height} width={width} />
