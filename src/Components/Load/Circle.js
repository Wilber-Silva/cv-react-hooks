import React from 'react'
import Skeleton from '@material-ui/lab/Skeleton'

export default ({ width, height, style }) => <Skeleton style={style} animation="wave" variant="circle" width={width} height={height} />