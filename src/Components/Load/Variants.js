import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

export default ({ width }) => 
(
    <div style={{ width }}>
        <Skeleton variant="text" height={40} />
        <Skeleton variant="text" height={40} />
        <Skeleton variant="rect" height={118} />
    </div>
)
