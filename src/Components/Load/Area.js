import React from 'react'
import Skeleton from '@material-ui/lab/Skeleton'

export default ({ width, height }) => <Skeleton variant="rect" height={height} width={width} />