import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Grid, Typography, Link } from '@material-ui/core'
import { Facebook, GitHub, LinkedIn } from '@material-ui/icons'

import Avatar from './Avatars/AvatarCircle'

const fontColor = '#FFF'

const useStyles = makeStyles((theme) => ({
            titleContainer: {
                textAlign: 'center',
                backgroundImage: 'url(/image/bg-back.jpg)',
                backgroundPosition: 'center',
                backgroundRepeat: 'no-repeat',
                minHeight: 500,
                width: '100%',
                padding: 20
            },
            title: {
                color: fontColor,
                [theme.breakpoints.down('md')]: {
                    marginTop: 15,
                    fontSize: 28
                }
            },
            subTitle: {
                color: fontColor,
                opacity: 0.85,
                [theme.breakpoints.down('md')]: {
                    marginTop: 5,
                    fontSize: 24
                }
            },
            links: {
                padding: 5     
            },
            link: {
                marginLeft: 8,
                marginRight: 8,
                color: fontColor,
                transition: 'color 0.3s ease-in-out 0s',
                '&:hover': {
                    color: '#00BFFF'
                },
                '& > svg': {
                    fontSize: 30,
                },
                [theme.breakpoints.down('md')]: {
                    '& > svg': {
                        fontSize: 26,
                    },
                },
            }
        }
    )
)

const Links = ({ className, linkClassName}) => (
    <div className={className}>
        <Typography variant='subtitle1'>
            <Link href='https://bitbucket.org/Wilber-Silva/' className={linkClassName} target='_blank'>
                <GitHub />
            </Link>

            <Link href='https://www.linkedin.com/in/wilber-silva-010a3336/' className={linkClassName} target='_blank'>
                <LinkedIn />
            </Link>

            <Link href='https://www.facebook.com/will.silva.142/' className={linkClassName} target='_blank'>
                <Facebook />
            </Link>
        </Typography>
    </div>
)

const TitleComponent = ({ loading }) => {
    const classes = useStyles()
    return (
        <Grid container className={classes.titleContainer} alignItems='center' justify='center' direction='column'>
            <Grid item>
                <Avatar borderColor="#00BFFF" alt='profile' largeX src='/image/avatar.jpg'/>
            </Grid>
            <Grid item container alignItems='center' justify='center' direction='column'>
                <Grid item>
                    <Typography variant='h2' className={classes.title}>
                        Wilber da Silva C. Costa
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography variant='h4' className={classes.subTitle}>
                        Desenvolvedor Full-Stack
                    </Typography>
                </Grid>
                <Grid item>
                    <br></br>
                    <Links className={classes.links} linkClassName={classes.link}/>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default TitleComponent
