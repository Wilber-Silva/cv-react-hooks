import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Avatar } from '@material-ui/core'


const useStyles = makeStyles((theme) => (
    {
        border: {
            borderWidth: 2,
            borderStyle: 'solid'
        },
        small: {
            width: theme.spacing(6),
            height: theme.spacing(6)
        },
        large: {
            width: theme.spacing(10),
            height: theme.spacing(10)
        },
        largeX: {
            width: theme.spacing(20),
            height: theme.spacing(20)
        }
    }
))

export default ({ largeX, large, small, alt, src, border = false, borderColor }) => {
    const classes = useStyles()
    let borderClassName = ''

    if (borderColor || border) borderClassName = classes.border

    if (small) return <Avatar alt={alt} src={src} className={`${classes.small} ${borderClassName}`} style={{borderColor}}/>
    if (large) return <Avatar alt={alt} src={src} className={`${classes.large} ${borderClassName}`} style={{borderColor}}/>
    if (largeX) return <Avatar alt={alt} src={src} className={`${classes.largeX} ${borderClassName}`} style={{borderColor}}/>

    return <Avatar alt={alt} src={src} className={`${classes.small} ${borderClassName}`} style={{borderColor}}/>
}